logLevel := Level.Warn

addSbtPlugin("org.scoverage"          %   "sbt-scoverage"         % "1.3.5")
addSbtPlugin("com.typesafe.sbt"       %   "sbt-git"               % "0.8.5")
