package edu.psu.its.sas.demo

object Demo {

  def absValue(n: Int) = if (n <= 0) -n else n

  def add(a: Int, b: Int) = a + b
}
