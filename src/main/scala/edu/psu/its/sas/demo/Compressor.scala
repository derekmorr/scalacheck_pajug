package edu.psu.its.sas.demo

import java.io.ByteArrayOutputStream
import java.nio.charset.StandardCharsets.UTF_8
import java.util.Base64
import java.util.zip.{Deflater, DeflaterOutputStream}

object Compressor {

  def compress(data: String, compressionLevel: Int): Array[Byte] = {
    val baos = new ByteArrayOutputStream()
    val deflater = new Deflater(compressionLevel)
    val compressedStream = new DeflaterOutputStream(baos, deflater)

    compressedStream.write(getUTFBytes(data))
    compressedStream.flush()
    compressedStream.close()
    baos.toByteArray
  }

  def base64Encode(data: String): String = {
    Base64.getEncoder.encodeToString(getUTFBytes(data))
  }

  def base64Decode(data: String): String = {
    val bytes = Base64.getDecoder.decode(getUTFBytes(data))
    new String(bytes)
  }

  private def getUTFBytes(str: String): Array[Byte] = str.getBytes(UTF_8.toString)
}
