package edu.psu.its.sas.demo.generators

import com.google.common.net.InetAddresses
import org.scalacheck.Arbitrary._
import org.scalacheck.{Arbitrary, Gen}
import org.scalacheck.Gen.{listOfN, oneOf}

case class User(firstName: String, lastName: String, age: Int)

object Generators {

  val genUser: Gen[User] = for {
    fname <- Gen.alphaStr
    lname <- Gen.alphaStr
    age   <- Gen.choose(1, 100)
  } yield User(fname, lname, age)

  def psuAccessId = for {
    first  <- Gen.alphaLowerChar
    middle <- Gen.alphaLowerChar
    last   <- Gen.alphaLowerChar
    hasNum <- Gen.frequency((1, false), (9, true))
    num    <- Gen.choose(1, 9999)
  } yield {
    val base = s"$first$middle$last"
    if (hasNum) s"$base$num" else base
  }

  val genIPv4Address = arbitrary[Int] map { i => InetAddresses.fromInteger(i) }

  val genIPv6Address = for {
    byteList <- listOfN(16, arbitrary[Byte])
  } yield InetAddresses.fromLittleEndianByteArray(byteList.toArray)

  val genInetAddress = oneOf(genIPv4Address, genIPv6Address)
  implicit val arbInetAddress = Arbitrary(genInetAddress)

}
