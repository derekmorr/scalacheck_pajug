package edu.psu.its.sas.demo

import edu.psu.its.sas.demo.Demo.add

/**
 * Property based tests for addition.
 */
class AddTest extends BaseTest {
  "Add" must {
    "be associative" in {
      forAll { (a: Int, b: Int, c: Int) =>
        // a + (b + c) == (a + b) + c
        add(a, add(b, c)) mustEqual add(add(a, b), c)
      }
    }

    "be commutative" in {
      forAll { (a: Int, b: Int) =>
        // (a + b) == (b + a)
        add(a, b) mustEqual add(b, a)
      }
    }

    "return the same value when zero is added to it" in {
      forAll { a: Int =>
        add(a, 0) mustEqual a
      }
    }

    "always be higher when you + 1" in {
      forAll { a: Int =>
        whenever(a != Int.MaxValue) {
          add(a, 1) must be > a
        }
      }
    }
  }
}
