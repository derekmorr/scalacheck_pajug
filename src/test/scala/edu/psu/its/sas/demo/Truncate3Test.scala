package edu.psu.its.sas.demo

import org.scalacheck.Gen

class Truncate3Test extends BaseTest {

  def truncate(str: String, length: Int): String = {
    if (length <= 0) {
      ""
    } else if (length < 3) {
      str.take(length)
    } else {
      if (str.length > length) {
        str.take(length - 3) + "..."
      } else {
        str.take(length)
      }
    }
  }

  "Truncate" must {
    "output must be no loner than specified length" in {
      forAll { (str: String, i: Int) =>
        whenever(i >= 0) {
          val output = truncate(str, i)
          output.length must be <= i
        }
      }
    }

    "return the original string if output length is greater than string length" in {
      forAll { (str: String, i: Int) =>
        whenever(i >= 0 && str.length <= i) {
          val output = truncate(str, i)
          output mustEqual str
        }
      }
    }

    "return the empty string if length is <= 0" in {
      forAll { (str: String, i: Int) =>
        whenever(i <= 0) {
          truncate(str, i) mustEqual ""
        }
      }
    }

    "add the ... if it needs to" in {
      forAll(Gen.alphaStr, Gen.choose(3, 500)) { (str: String, i: Int) =>
        whenever(str.length > i && i > 3) {
          truncate(str, i) must endWith ("...")
        }
      }
    }
  }

}











