package edu.psu.its.sas.demo

import scalaz.NonEmptyList
import edu.psu.its.sas.demo.generators.NelGenerators._

/**
  * Some math tests
  */
class MathNelTests extends BaseTest {

  def average(numbers: NonEmptyList[Int]): Double = {
    val sum = numbers.map { _.toLong }.list.sum
    val length = numbers.list.length.toDouble
    sum / length
  }

  "compute an average" in {
    forAll { numbers: NonEmptyList[Int] =>
      val min = numbers.list.min.toDouble
      val max = numbers.list.max.toDouble

      average(numbers) must be >= min
      average(numbers) must be <= max
    }
  }
}
