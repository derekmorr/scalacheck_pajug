package edu.psu.its.sas.demo


/**
 * Some math tests
 */
class MathTest extends BaseTest {

  def average(numbers: List[Int]): Double = {
    val sum = numbers.map { _.toLong }.sum
    val length = numbers.length.toDouble
    sum / length
  }

  "compute an average" in {
    forAll { numbers: List[Int] =>
      val min = numbers.min.toDouble
      val max = numbers.max.toDouble

      average(numbers) must (be >= min and be <= max)
    }
  }
}
