package edu.psu.its.sas.demo.generators

import org.scalacheck.Arbitrary._
import org.scalacheck.{Arbitrary, Gen}

import scalaz.NonEmptyList

object NelGenerators {

  implicit def NonEmptyListGen[A](implicit a: Arbitrary[A]): Gen[NonEmptyList[A]] = {
    for {
      first     <- arbitrary[A]
      remaining <- arbitrary[List[A]]
    } yield NonEmptyList(first, remaining:_*)
  }

  implicit def NonEmptyListArb[A](implicit a: Arbitrary[A]): Arbitrary[NonEmptyList[A]] = Arbitrary(NonEmptyListGen(a))

}
