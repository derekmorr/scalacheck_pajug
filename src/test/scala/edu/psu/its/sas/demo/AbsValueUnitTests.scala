package edu.psu.its.sas.demo

import edu.psu.its.sas.demo.Demo.absValue

class AbsValueUnitTests extends BaseTest {
  
  "AbsValue" must {
//    "return 1 for 1" in {
//      absValue(1) mustEqual (1)
//    }
//
//    "return 0 for 0" in {
//      absValue(0) mustEqual (0)
//    }
//
//    "return Int.MaxValue for Int.MaxValue" in {
//      absValue(Int.MaxValue) mustEqual (Int.MaxValue)
//    }

    "must be non-negative" in {
      import scala.util.Random

      for (i <- 1 to 10) {
        val randomNumber = Random.nextInt    // <-- random generator
        val output = absValue(randomNumber)

        output must be >= 0     // <-- property
      }
    }

    "must have the correct value" in {
      import scala.util.Random

      for (i <- 1 to 10) {
        val randomNumber = Random.nextInt    // <-- random generator
        val output = absValue(randomNumber)

        output must (equal(randomNumber) or equal(-randomNumber))
      }
    }


  }
}
