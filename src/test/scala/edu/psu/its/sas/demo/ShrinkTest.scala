package edu.psu.its.sas.demo

class ShrinkTest extends BaseTest {

  def brokenReverse(xs: List[Int]) = {
    if (xs.length > 4) xs.reverse else xs
  }

  "must reverse a list" in {
    forAll { elements: List[Int] =>
      whenever(elements.nonEmpty) {
        elements.last mustEqual (brokenReverse(elements).head)
      }
    }
  }

}
