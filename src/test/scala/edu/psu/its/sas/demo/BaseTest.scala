package edu.psu.its.sas.demo

import org.scalactic.TypeCheckedTripleEquals
import org.scalatest.prop.GeneratorDrivenPropertyChecks
import org.scalatest.{DoNotDiscover, MustMatchers, WordSpec}

/**
  * Base class for tests.
  */
@DoNotDiscover
class BaseTest extends WordSpec
  with MustMatchers
  with GeneratorDrivenPropertyChecks
  with TypeCheckedTripleEquals {

}
