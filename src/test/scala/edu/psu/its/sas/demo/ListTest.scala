package edu.psu.its.sas.demo

/**
 * Property based tests for Lists.
 */
class ListTest extends BaseTest {

  "Lists" must {
    "retain all elements of two concatted lists" in {
      forAll { (listA: List[Int], listB: List[Int]) =>
        val concattedList = listA ++ listB
        concattedList must contain allElementsOf listA
        concattedList must contain allElementsOf listB
      }
    }

    "retain the size of two concatted lists" in {
      forAll { (listA: List[Int], listB: List[Int]) =>
        val concattedList = listA ++ listB
        concattedList.size mustEqual listA.size + listB.size
      }
    }

    "roundtrip reverse must return the starting list" in {
      forAll { listA: List[Int] =>
        listA.reverse.reverse mustEqual listA
      }
    }

    "sorting" must {
      "stay sorted once sorted" in {
        forAll { listA: List[Int] =>
          val sortedList = listA.sorted
          sortedList.sorted mustEqual sortedList
        }
      }

      "not lose elements" in {
        forAll { listA: List[Int] =>
          listA.sorted must contain theSameElementsAs listA
        }
      }

      "preserve count" in {
        forAll { listA: List[Int] =>
          listA.sorted.length mustEqual listA.length
        }
      }
    }

  }
}
