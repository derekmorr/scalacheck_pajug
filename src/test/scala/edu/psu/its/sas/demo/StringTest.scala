package edu.psu.its.sas.demo


/**
 * Property based tests for strings.
 */
class StringTest extends BaseTest {

  "Strings" must {
    "lower must be the same as upper-then-lower" in {
      forAll { string: String =>
        val upperThenLower = string.toUpperCase().toLowerCase()
        val lower = string.toLowerCase

        upperThenLower mustEqual lower
      }
    }

    "concat two values properly" in {
      forAll { (strA: String, strB: String) =>
        strA + strB must startWith (strA)
      }
    }

    "concat" must {
      "preserve length" in {
        forAll { (strA: String, strB: String) =>
          val concatLen = (strA + strB).length
          concatLen must be > strA.length
          concatLen must be > strB.length
        }
      }
    }
  }
}
