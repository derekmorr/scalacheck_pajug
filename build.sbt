name := "scalacheck-demo"

version := "1.0"

scalaVersion := "2.11.8"

lazy val root = (project in file("."))

libraryDependencies ++= {
  Seq(
    "org.scalaz"          %% "scalaz-core"      % "7.1.8"   % Compile,
    "com.google.guava"    %  "guava"            % "19.0"    % Test,
    "org.scalacheck"      %% "scalacheck"     	% "1.13.2"  % Test,
    "org.scalatest"       %% "scalatest"      	% "3.0.0"   % Test,
    "org.pegdown"         %  "pegdown"        	% "1.6.0"   % Test
  )
}

scalacOptions ++= Seq(
  "-feature", "-unchecked", "-deprecation", "-Xcheckinit", "-Xlint",
  "-Xfatal-warnings", "-g:line", "-Ywarn-dead-code", "-Ywarn-numeric-widen")

javacOptions ++= Seq("-source", "1.8", "-target", "1.8", "-Xlint")

// don't let Ctrl-C exit
cancelable in Global := true

// generate HTML reports for tests
(testOptions in Test) += Tests.Argument(TestFrameworks.ScalaTest, "-h", "target/report")

// show test durations
(testOptions in Test) += Tests.Argument(TestFrameworks.ScalaTest, "-oD")

// run tests in parallel
parallelExecution in Test := true

// cache dependency resolution information
updateOptions := updateOptions.value.withCachedResolution(true)

enablePlugins(GitBranchPrompt)
